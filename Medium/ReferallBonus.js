/*Question: build referral bonus system
	I/p: { a: b, b: c, c: d, e: f, k: z }
            O/P: 
 a -> 10 + 9 + 8  = 27
 b -> 10 + 9  = 19
 c -> 10
 e -> 10
 k -> 10
*/

//Approach
// The given problem is clearly a example of recursion. 
// so we recursively call the calculatebonus function until 10 referalls are over or there is no further indirect referall possible.

const json = {"a": "b", "b":"c", "c":"d", "e":"f", "k": "z" }
finalObject ={}
for(let key of Object.keys(json))
{
	 finalObject[key] = calculateTotalBonus(json,10,key);
}
console.log(finalObject);
function calculateTotalBonus(object,max,curr)
{
 	 if(max == 0 || object[curr]==undefined)
   return 0
   if(object[curr]!=undefined)
   return max+ calculateTotalBonus(object,max-1,object[curr]);
}

