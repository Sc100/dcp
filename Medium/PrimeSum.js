//Given an even number (greater than 2), return two prime numbers whose sum will be equal to the given number


function solve(A){

    const domain = sieve(A);
    let primeMap = {}
    domain.map((val,index)=>{
        primeMap[val]=1;
    })
    // console.log(domain)

    let output=[]
    for(i=0;i<domain.length;i++){
        if(primeMap[domain[i]]!=undefined
        && primeMap[A-domain[i]]!=undefined){
            return [domain[i],A-domain[i]]
        }
    }
    // Complexity - O(n log (log(n)))
    function sieve(N){
        const primes  = []
        const isPrime  = Array(N).fill(true);
        isPrime[0] = false
        isPrime[1] = false
        for(i = 2; i< N ; i++){
            if(isPrime[i]){
                primes.push(i)
            }
            for(j = i*i ;j <= N;j+=i){
                isPrime[j] = false;
            }

        }
        return primes;
    }
    

}

