/*Given a list of integers, return the largest product that can be made by multiplying any three integers.

For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.

You can assume the list has at least three integers.*/


// Approach 
// Naive approach is to find all triplets using O(n3) time 
// Optimised approach would be first sorting the array and then checking the last three elements product which will be greater if elements are positive. Even multiplying two negative numbers will also result in positive , so considering both the results and
// returning max of them will be the solution.

function solve(nums){
    nums.sort((a,b) =>{
        if(a>b)return 1
        if(a<b) return -1
        return 0
     });
     len = nums.length
    return Math.max(nums[0]*nums[1]*nums[len-1], nums[len-1]*nums[len-2]*nums[len-3])

}

console.log(solve([-100,-98,-1,2,3,4]))