//Given the root of a binary tree, return a deepest node. For example, in the following tree, return d.

//Approach
// We can do a level order traversal in the tree and return the node in the last level which is last in the queue


function solution(root){
    if(!root){
        return null;
    }
    let nodes = []
    nodes.push(root)
    while(nodes.length > 0)
    {
    current = nodes.pop();
    if(current.left)
    nodes.push(current.left)
    else if(current.right)
    nodes.push(current.right)
    
    }
return current;
}

// Time complexity : O(n)
// Space complexity : O(n)