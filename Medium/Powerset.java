package Medium;

/*The power set of a set is the set of all its subsets. Write a function that, given a set, generates its power set.

For example, given the set {1, 2, 3}, it should return {{}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}


#Approach 1 
#For finding the subsets of a set, Check all the combinations, by running two for loops which will not be a time efficient solution.this will be brute force way of doing the solution.
#Approach 2
#using backtracking to get all the possible subsets
#Approach 3
#In the time efficient approach , Bit manipulation can be used as
#00,01,10,11 are four possible numbers if we have only 2 bits
#000,001,010,011,100,110,111 are possible numbers if we have 3 bits.
#Using this pattern we can have different combinations of sets
*/

import java.io.*;
import java.util.*;
import java.math.*;

public class Powerset
{
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);
        String[] line = input.nextLine().split(" ");
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        ArrayList<List<Integer>> output = new ArrayList<>();
        for(String number:line)
        {
            numbers.add(Integer.parseInt(number));
        }
        ArrayList<Integer> temp = new ArrayList<>();
        int i,j;
        for(i=1 ;i < Math.pow(2,line.length);i++)
        {
            temp = new ArrayList<>();
           
            for(j = 0 ;j<line.length;j++)
            {   
                // System.out.println();
                if((i & (1 << j))>0)
                {
                    temp.add(numbers.get(j));
                    
                }

            }
            
            output.add(temp);
            
        }
        displayElements(output);

    }
    public static  void displayElements(List<List<Integer>>list)
    {
        int i;
        System.out.println("[]");
        for(List<Integer> entry: list)
        {
            
            System.out.print("[ ");
            for(i=0;i<entry.size();i++){
            System.out.print(entry.get(i)+" ");
            }
            System.out.print("]");
            System.out.println();
  
        }
    }

    
}



