//Traverse JSON to print all key value pairs and if it has any parent 
// append a dot infront of it to print the key and corresponding value

//Approach 
// AS we have to consider inner nestings of JS as well , solving with recursion will be a good idea.


const json = {
    a: 1,
    b: 2,
    name: 'sarath',
    college: 'jntu',
    skills: [
        'js',
      'java'
    ],
    address: {
        street: 'college road',
      innerAddress: {
          pincode: '500001',
        tags: ['food street', 'tech park']
      }
    },
    isMajor: true,
  };
  
  print(json,undefined)
  
  function print(object,parent)
  {
      for(let key of Object.keys(object))
    {
     if(typeof(object[key])=="object" && !Array.isArray(object[key]))
     print(object[key],key);
     else
     {
     if(parent==undefined)
     console.log(key +" -> "+ object[key])
     else
         console.log(parent+"."+key+" -> "+ object[key]);
     }
    }
  }