/*Given a list of integers and a number K, return which contiguous elements of the list sum to K.

For example, if the list is [1, 2, 3, 4, 5] and K is 9, then it should return [2, 3, 4], since 2 + 3 + 4 = 9.
*/

function solve(nums, k) {
  let i = 0,
    j = 0;
  let sumSoFar = 0;
  let output = [];
  while (i < nums.length) {
    while (j < nums.length && sumSoFar < k) {
      sumSoFar += nums[j];
      output.push(nums[j++]);
    }
    if (sumSoFar == k) {
      return output;
    }
    sumSoFar -= nums[i++];
    output.shift();
  }
}

console.log(solve([1, 2, 3, 4, 5], 9));

//Space complexity - O(1)
// Time complexity - O(N)