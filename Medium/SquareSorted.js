// Given a sorted list of integers, square the elements and give the output in sorted order.

//For example, given [-9, -2, 0, 2, 3], return [0, 4, 4, 9, 81].





var sortedSquares = function(nums) {
    let j = 0,k=0;s=0;
    let result = Array(nums.length);
    nums.map((val,i)=>{
        if(nums[i]<0){
            j++;
        }
        nums[i] = nums[i]*nums[i];
        
    })
    k=j-1;
    while(k>=0 && j<nums.length){
        if(nums[k] < nums[j]){
            result[s++] = nums[k--];
        
        }
        else{
            result[s++] = nums[j++];
        }
        
    }
    while(k>=0)
        result[s++] = nums[k--];
    
    while(j<nums.length)
        result[s++] = nums[j++]
    
    return result;
    
    
};

console.log(sortedSquares([-9,-1,0,2,3]));