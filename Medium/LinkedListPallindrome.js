// Determine whether a doubly linked list is a palindrome. What if it’s singly linked?

//For example, 1 -> 4 -> 3 -> 4 -> 1 returns True while 1 -> 4 returns False.//

//Approach
// The problem can be solved easily if its doubly linked list by initialising a pointer at the start and another pointer at the end.
// As we can move towards the left,we can go on checking until start < last is true
// We can check whether the characters are equal and if they are not equal at any place , we can break and return

// What if its singly linked list
// Approach
// we can find the reverse of linkedlist and compare the linkedlist at every node.
// but this uses O(n) space
// We can try to solve this in O(1) space and O(n) time
// we first find middle of linked list and reverse the second half
// compare first and second halves to check whether the linked list is pallindrome or not.

//Time complexity - O(n)
// Space complexity - O(1)

var isPalindrome = function (head) {
  if (head.next == null) {
    return true;
  }

  function findMiddle(head) {
    let slow = head;
    let fast = head;
    while (fast && fast.next != null) {
      slow = slow.next;
      fast = fast.next.next;
    }

    return slow;
  }
  let middle = findMiddle(head);
  console.log(middle.val);
  if (middle.next == null) {
    if (head.val == head.next.val) return true;
    else return false;
  }
  let second = findReverseHalf(middle);
  while (second && head.val == second.val) {
    head = head.next;
    second = second.next;
  }
  if (second === null) {
    return true;
  } else {
    return false;
  }

  function findReverseHalf(node) {
    console.log(node.val);
    let curr = node;
    let prev = null;
    let next = curr.next;
    while (curr != null) {
      next = curr.next;
      curr.next = prev;
      prev = curr;
      curr = next;
    }
    if (prev) return prev;
    else return curr;
  }
};

//isPalindrome();



