function List(size){
  elements = new Array(size)
  MAX_SIZE = size
  index = -1
  sumUptoI = []
  
  
   function push(val){
   
   if(isFull())
   throw new Error("List is full")

   else
    elements[++index] = val
    if(index == 0)
    sumUptoI[0] = val
    else
    sumUptoI[index] = sumUptoI[index-1]+ val
   }
   
   
   function at(ind){
    if(ind >=0 && ind <= index)
    return elements[ind]
    else 
    throw new Error("Enter a valid index")
   }
   
   
   function pop(){
   	elements[index] = 0
    sumUptoI[index] = 0
    return elements[index--]
   }
   
   
   function isFull(){
     return index < MAX_SIZE-1 ? false : true
   }
   
   
   function sumSoFar(start,end){
   let sum = 0 
    if(start >=0 && end <= index && start-end<=0){
     if(start == 0)
     return sumUptoI[end]
     else
    return sumUptoI[end] - sumUptoI[start-1]
    }
    else
    throw new Error("Enter valid indexes")
   	 
   }
   return {
   push : push,
   pop : pop,
   sumSoFar : sumSoFar,
   at : at
   }
   
}

// Time Complexity 
// push - O(1)
// pop - O(1)
// at - O(1)
// isFull - O(1)
// sumSoFar - O(1)

// Space complexity 
// push - O(1)
// pop - O(1)
// at - O(1)
// isFull - O(1)
// sumSoFar - O(n)


l = new List(5)
l.push(10)
l.push(12)
l.push(13)
l.push(14)
l.push(15)
l.pop()
console.log(l.sumSoFar(0,2))
console.log(l.sumSoFar(1,3))
console.log(l.sumSoFar(2,2))
console.log(l.at(2))