// Return all different characters in the strings

let str1 = "abcde"
let str2 = "abcdef"

function solve(str1,str2){

let visited = Array(26).fill(0)

for(let i = 0 ;i < str1.length ; i++){
	visited[str1.charCodeAt(i)- 97]+=1
}

for(let i= 0;i<str2.length ; i++){
  visited[str2.charCodeAt(i)-97]-=1
}
output = new Set()

visited.map((val,index)=>{
   if(val!=0)
   output.add(String.fromCharCode(97+index))
     
})

output = [...output]

return output

}
console.log(solve(str1,str2))