const json = {
    a: 1,
    b: 2,
    name: 'sarath',
    college: 'jntu',
    skills: [
        'js',
      'java'
    ],
    address: {
        street: 'college road',
      innerAddress: {
          pincode: '500001',
        tags: ['food street', 'tech park']
      }
    },
    isMajor: true,
  };
  
  print(json,undefined)
  
  function print(object,parent)
  {
      for(let key of Object.keys(object))
    {
     if(typeof(object[key])=="object" && !Array.isArray(object[key]))
     print(object[key],key);
     else
     {
     if(parent==undefined)
     console.log(key +" -> "+ object[key])
     else
         console.log(parent+"."+key+" -> "+ object[key]);
     }
    }
  }