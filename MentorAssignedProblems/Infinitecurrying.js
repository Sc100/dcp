//multiply(1)(2)(3)(4)(5) = 120

/* const multiply = function(a)
{
  return function(b)
  {
    if(b)
    return multiply(a*b)
    else
    return a
  }
} */
const multiply = (a) => (b) => b ? multiply(a*b) : a
console.log(multiply(1)(2)(3)(4)(5)(6)())