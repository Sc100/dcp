const dependencyArray = [[1,2],[2,3],[3,5]] 
const queryArray = [[1,3],[2,5],[2,3]]
const output =[]
function solve(dependencyArray, queryArray){
    let arrayMap = {}
    createMap(dependencyArray);
    function createMap(dependencyArray){
        dependencyArray.map((val)=>{
            arrayMap[val[0]] = val[1]
        })
    }
    queryArray.map((val)=>{
        let flag = false;
        let key = val[0]
        while(arrayMap[key]!=undefined ){
            
            if(arrayMap[key]== val[1]){
                console.log(key);
                output.push(true);
                flag = true;
                break;
            }
            key = arrayMap[key]
        }
    
        if(flag==false){
            console.log(key)
            output.push(false);
        }
       
        
    })
    return output;

}
console.log(solve(dependencyArray, queryArray));