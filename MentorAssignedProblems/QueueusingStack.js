// stack 
function Stack (){
    
    let elements =[];
    let top = -1;
      
    
    function push(val){
        /* console.log(val) */
			 elements[++top] = val
       
    }
    
    function pop(){
       if(isEmpty()){
       throw new Error("Stack is Empty")
       }
       
       else
       return elements[top--]
    }
    
    function isEmpty(){
    	return top === -1 ? true : false
    }
    
    function peek(){
    if(isEmpty())
    throw new Error("No elements in the stack")
    else
    return elements[top-1]
    
    }
    
    return {
    	push: push,
      pop : pop,
      peek : peek,
      top : top,
      isEmpty : isEmpty
    }
}

function Queue()
{
   s1 = new Stack()
   s2 = new Stack()
   function enqueue(val)
   {
      while(!s2.isEmpty())
      s1.push(s2.pop())
      
   		s1.push(val)
   }
   function dequeue(val)
   {
     
      while(!s1.isEmpty()){
      
        s2.push(s1.pop())
      }
      
      return s2.pop()
      
   }
   return {
   enqueue : enqueue,
   dequeue: dequeue
   }
}

q = new Queue()
q.enqueue(10)
q.enqueue(20)
console.log(q.dequeue())
console.log(q.dequeue())
q.enqueue(30)
q.enqueue(40)
q.enqueue(50)
q.enqueue(60)
q.enqueue(70)
q.enqueue(90)
console.log(q.dequeue())
console.log(q.dequeue())









