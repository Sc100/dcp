// Queue Implementation
function Queue(){
 
  let elements = []
  let rear = 0
  let front = 0
  
  function enqueue(val){
    elements[rear++] = val
  }
  
  
  function dequeue(){
  let val;
  
  if(isEmpty())
    throw new Error("No more elements in the Queue")  
  else {
    val  = elements[front]
    front+=1
  }
  return val
  
  }
  
  function isEmpty(){
    return rear-front ==0 ? true: false
  }
  
  function peek(){
  if(!isEmpty())
    return elements[front]
  else
    throw new Error("No Elements in the queue")
  }
  
  return {
    enqueue : enqueue,
    dequeue : dequeue,
    peek : peek
  }
  
 }
 // Time complexity 
 // enqueue - O(1)
 // dequeue - O(1)
 // peek - O(1)
 // isEmpty - O(1)
 
 //Space complexity 
 // enqueue - O(1)
 // dequeue - O(1)
 // peek - O(1)
 // isEmpty - O(1)
 
 
 q = new Queue()
 q.enqueue(10)
 console.log(q.dequeue())
 /* console.log(q.peek()) */
 q.enqueue(50)
 q.enqueue(20)
 q.enqueue(30)
 console.log(q.peek())
 q.enqueue(34)
 q.enqueue(44)
 q.enqueue(35)
 q.enqueue(55)
 console.log(q.peek())
 q.dequeue()
 console.log(q.peek())