// Queue Implementation
function Queue(){
 
    let elements = []
    let rear = 0
    let front = 0
    
    function enqueue(val){
      elements[rear++] = val
    }
    
    
    function dequeue(){
    let val;
    
    if(isEmpty())
        throw new Error("No more elements in the Queue")  
    else {
        val  = elements[front]
        front+=1
    }
    return val
    
    }
    
    function isEmpty(){
      return rear-front ==0 ? true: false
    }
    
    function peek(){
    if(!isEmpty())
        return elements[rear-1]
    else
        throw new Error("No Elements in the queue")
    }
    
    function size(){
      return rear-front
    }
    
    return {
      enqueue : enqueue,
      dequeue : dequeue,
      peek : peek,
      size : size
    }
    
   }
   
   function Stack()
   {
       q = new Queue()
       sup_q = new Queue()
       function push(val)
       {
         q.enqueue(val) 
       }
       function pop(val)
       {
               
             while(q.size() != 1){
              sup_q.enqueue(q.dequeue())
           }
           val = q.dequeue()
           temp = q
           q = sup_q
           sup_q = q 
           return val
       }
       function peek(){
       return q.peek()
       }
       
       return {
       push : push,
       pop : pop,
       peek : peek
       }
   }
   
   s = new Stack()
   s.push(1)
   console.log(s.peek())
   s.push(2)
   console.log(s.peek())
   console.log(s.pop())
   console.log(s.pop())
   s.push(3)
   console.log(s.pop())