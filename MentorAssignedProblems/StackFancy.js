// stack 
function Stack (){
    
    let elements =[];
    let top = 0;
    let even,odd;
    
    
    function push(val){
       p = top
			 elements[top] = val
       top+=1
       
    }
    
    function pop(){
       if(isEmpty())
       throw new Error("Stack is Empty")
       else
       return elements[top--]
    }
    
    
    function isEmpty(){
    	return top==0 ? true : false
    }
    
    
    function peek(){
    if(isEmpty())
    throw new Error("No elements in the stack")
    else
    return elements[top-1]
    
    }
    
    function getAlternativePeek() {
       let ans;
       //console.log(top
       if(top == -1)
       {
         throw new Error("Stack is Empty")
       }
       if(top%2 == 0){
           if(even == undefined){
               even = top-1
              ans = elements[even]
           }
           else{
                even = even - 2
               if( even < 0)
                even = top - Math.abs(even)
                ans = elements[even]
           }
        }
        else
        {
          
           if(odd == undefined)
           {
              
           	  odd = top-2
             if(odd < 0 ){
                odd = top - Math.abs(odd)
             }
             ans =  elements[odd]
             
           }
           else
           {
             odd = odd-2
             if(odd < 0)
              odd = top - Math.abs(odd)
             ans = elements [odd]
           }
        }
       return ans 
      }
    			
    
    return {
    	push: push,
      pop : pop,
      peek : peek,
      getAltPeak : getAlternativePeek 
    }
}

// Time complexity 
// push - O(1)
// pop - O(1)
// isEmpty - O(1)
// peek - O(1)

// Space complexity 
// push - O(1)
// pop - O(1)
// isEmpty - O(1)
// peek - O(1)






s = new Stack()
s.push(1)
s.push(2)
s.push(3)
s.push(4)
console.log(s.getAltPeak())
s.pop()
console.log(s.getAltPeak())
s.push(4)
console.log(s.getAltPeak())
console.log(s.getAltPeak())
s.pop()
console.log(s.getAltPeak())
s.push(5)
console.log(s.getAltPeak())
