// stack 
function Stack (){
    
   let elements =[];
   let top = 0;
     
   
   function push(val){
         elements[top] = val
      top+=1
   }
   
   function pop(){
      if(isEmpty())
      throw new Error("Stack is Empty")
      else
      return elements[top--]
   }
   
   function isEmpty(){
      return top==0?true:false
   }
   
   function peek(){
   if(isEmpty())
   throw new Error("No elements in the stack")
   else
   return elements[top-1]
   
   }
   
   
   return {
      push: push,
     pop : pop,
     peek : peek
   }
}

// Time complexity 
// push - O(1)
// pop - O(1)
// isEmpty - O(1)
// peek - O(1)

// Space complexity 
// push - O(1)
// pop - O(1)
// isEmpty - O(1)
// peek - O(1)






s = new Stack()

s.push(1)
/* console.log(s.elements) */
console.log(s.peek())
s.push(2)
s.push(3)
s.push(4)
console.log(s.peek())
s.pop()
console.log(s.peek())



