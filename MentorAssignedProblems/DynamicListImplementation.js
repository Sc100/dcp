function List(size){
    elements = new Array(size)
    MAX_SIZE = size
    index = -1
    sumUptoI = []
    
    
     function push(val){
      elements[++index] = val
      if(index == 0)
      sumUptoI[0] = val
      else
      sumUptoI[index] = sumUptoI[index-1]+ val
     }
     
     
     function at(ind){
      if(ind >=0 && ind <= index)
      return elements[ind]
      else 
      throw new Error("Enter a valid index" + index)
     }
     
     
     function pop(){
         elements[index] = 0
      sumUptoI[index] = 0
      return elements[index--]
     }
     
     
     function isFull(){
       return index < MAX_SIZE-1 ? false : true
     }
     
     
     function sumSoFar(start,end){
     let sum = 0 
      if(start >=0 && end <= index && start-end<=0){
       if(start == 0)
       return sumUptoI[end]
       else
      return sumUptoI[end] - sumUptoI[start-1]
      }
      else
      throw new Error("Enter valid indexes")
          
     }
     
     function getElements(){
     return elements
     }
     
     return {
     push : push,
     pop : pop,
     sumSoFar : sumSoFar,
     at : at,
     isFull : isFull,
     getElements : getElements
     
     }
     
  }
  
  // Time Complexity 
  // push - O(1)
  // pop - O(1)
  // at - O(1)
  // isFull - O(1)
  // sumSoFar - O(1)
  
  // Space complexity 
  // push - O(1)
  // pop - O(1)
  // at - O(1)
  // isFull - O(1)
  // sumSoFar - O(n)
  function DList() {
  
       MAX_CAPACITY = 10
       d_list = new List(MAX_CAPACITY)
     function pushCheck(val)
     {
       if(d_list.isFull()){
        reCreate();
        d_list.push(val);
      }
      else
      d_list.push(val)
      
     }
     
     
     function reCreate(){
       old_elements = d_list.getElements()
       let new_elements = new List(MAX_CAPACITY*2)
       old_elements.map((val) =>
       {
               new_elements.push(val)
          
       })
       d_list = new_elements
       
         }
     
     
      
     return {
     push : pushCheck,
     at : d_list.at,
     pop : d_list.pop,
     sumSoFar : d_list.sumSoFar
     }
  }
  
  // Time complexity 
  // reCreate() - O(n)
  
  //Space complexity 
  // reCreate() - O(n)
  
  
  
  
  l = new DList()
  l.push(10)
  l.push(11)
  l.push(12)
  l.push(13)
  l.push(14)
  l.push(15)
  l.push(16)
  l.push(17)
  l.push(18)
  l.push(19)
  l.push(20)
  l.pop()
  console.log(l.at(7))
  l.push(12)
  l.push(13)
  l.push(14)
  l.push(15)
  l.push(16)
  l.push(17)
  l.push(18)
  l.push(12)
  l.push(13)
  l.push(14)
  l.push(15)
  l.push(16)
  l.push(17)
  l.push(18)
  console.log(l.at(20))
  console.log(l.sumSoFar(0,20))
  
  
  