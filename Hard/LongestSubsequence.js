/*
This problem was asked by Microsoft.

Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

For example, given [100, 4, 200, 1, 3, 2], the longest consecutive element sequence is [1, 2, 3, 4]. Return its length: 4.

Your algorithm should run in O(n) complexity.
*/

function solve(nums) {
  if (nums.length == 0) {
    return 0;
  }
  const numsMap = new Map();
  nums.map((val, index) => {
    numsMap.set(val, 0);
  });
  longestSeq = 0;
  const iterator1 = numsMap[Symbol.iterator]();

  for (const item of iterator1) {
    if (numsMap.get(item[0] - 1) == undefined) {
      current = item[0];
      localMax = 1;
      current += 1;
      while (numsMap.get(current) != undefined) {
        current += 1;
        localMax += 1;
      }
      longestSeq = Math.max(localMax, longestSeq);
    }
  }
  return longestSeq;
}
