#Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

#For example, if our input was[1, 2, 3, 4, 5], the expected output would be[120, 60, 40, 30, 24]. If our input was[3, 2, 1], the expected output would be[2, 3, 6].

#Follow-up: what if you can't use division?

# Approach 1
# solve this question easily using a division operator, but One are not alloOned to use division operator , So One can using prefix sum logic to arrive at a solution
# solve this question by using two arrays left and right, where left[i] contains the product of numbers to the left of i and similary for the right.
# Finally output[i] = left[i] * right[i] to get the output array.

#Approach 2
#Eliminate the use of two arrays to get the final output array which would be a more space efficient solution.

numbers = list(map(int,input().split()))
start = 1
output = [1 for i in range(len(numbers))]
for i in range(len(numbers)):
    output[i]  = start
    start *= numbers[i]
start = 1
for i in range(len(numbers)-1,-1,-1):
    output[i] *= start
    start   *= numbers[i]
print(*output)
