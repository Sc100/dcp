/* You are in an infinite 2D grid where you can move in any of the 8 directions

 (x,y) to 
    (x-1, y-1), 
    (x-1, y)  , 
    (x-1, y+1), 
    (x  , y-1),
    (x  , y+1), 
    (x+1, y-1), 
    (x+1, y)  , 
    (x+1, y+1) 
You are given a sequence of points and the order in which you need to cover the points.. Give the minimum number of steps in which you can achieve it. You start from the first point.

*/


function solve(A,B){
    if(A.length  == 1)
        return 0
        else{
            let countSteps  = 0 
            for(i = 0;i<A.length-1;i++){
                x1= A[i]
                y1 = B[i]
                x2 = A[i+1]
                y2= B[i+1]
                countSteps+= Math.max(Math.abs(x2-x1),Math.abs(y2-y1))
            }
            return countSteps
        }
}

//solve(A,B)