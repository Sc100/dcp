/**
 * class Tree {
 *   constructor(val, left=null, right=null) {
 *     this.val = val
 *     this.left = left
 *     this.right = right
 *   }
 * }
 */
 function solve(root,val){
        if(root==null)
        return false
        if(root.val == val )
        return true
        else if(root.val < val)
        return solve(root.right, val)
        else
        return solve(root.left,val)
        
    }
