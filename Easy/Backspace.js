
/*Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.

Note that after backspacing an empty text, the text will continue empty.*/
var backspaceCompare = function(s, t) {
    function check(s)
    {
        var convertedstring =[]
        for(i=0;i<s.length;i++)
        {
            if(s[i]!='#')
                convertedstring.push(s[i])
            else if(s[i]=='#' && convertedstring.length > 0)
                convertedstring.pop()
            
            
        }
        
        return convertedstring
    }
    first = check(s)
    second = check(t)
    return  first.length == second.length && first.every((val,index) => val === second[index])
    
};