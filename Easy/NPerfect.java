package Easy;

import java.util.Scanner;

/*This problem was asked by Microsoft.

A number is considered perfect if its digits sum up to exactly 10.

Given a positive integer n, return the n-th perfect number.

For example, given 1, you should return 19. Given 2, you should return 28.*/

//Approach 1
// In this problem we have to return nth perfect number 
// Brute force approach is running a for loop over numbers from 19 directly and checking for perfect number until we find n 
// Approach 2
// In this approach , by observation 19,28,37,46.... all the numbers have the common difference of 9 which shows that they are in AP, so starting from 19 we can add 9 to each of these numbers to gain the advantage of time with checking the sum of digits.
public class NPerfect
{
    public static void main(String args[])
    {
        int n;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        int[] twoDigit ={19,28,37,46,55,64,73,82,91};
        int start = 100;
        if(n<10)
        System.out.println(twoDigit[n-1]);
        else
        {
            n-=9;
            while(n>0)
            {
                if(sumOfDigits(start)==10)
                {
                    n--;
                    
                }
                start+=9;
                
            }
            System.out.println("Nth perfect numbers is "+ String.valueOf(start-9));
        }
	}
	static int sumOfDigits(int num)
	{
	    int sum = 0;
	    while(num > 0)
	    {
	        sum = sum + num%10;
	        num = num/10;
	    }
	    return sum;
	} 
}



// 19 28 37 46 55 64 73 82 91 
// 