#Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
#For example, given[10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17




#Apart from the Brute Force approach which can be done in two passes


#Approach 1
#Calculate the difference between target element and the current element in a loop and search whether the difference exists in the list or not using python's in operator (which takes o(n) time complexity)
#Time complexity: O(n^2)
#Space complexity:O(1)

#Approach 2
#Intuition: This approach is a improvement of Approach 1 ,Optimise the search time , For search in O(1) always prefer maps. 

#Push the current element into a python dictionary(map), And the difference for any element is already present in map, then the solution is possible for this question.



numbers = list(map(int,input().split()))
target_sum = int(input())
difference_map = {}
found_pair = 0
for i,num1 in enumerate(numbers):
    try:
        index = difference_map[target_sum-num1]
        found_pair = 1
        break

    except:
        difference_map[num1] = i
if found_pair:
    print("Pair is found at ",end=" ")
    print(i-1,index)



