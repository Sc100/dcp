//Print the nodes in a binary tree level-wise. For example, the following should print 1, 2, 3, 4, 5.

//   1
//  / \
// 2   3
//    / \
//   4   5

function solve(root){
    output = levelOrder(root);
    function levelOrder(root){
        queue = []
        queue.push(root)
        while(queue.length > 0){
            node = queue.shift()
            output.push(node.val);
            if(node.left)
            queue.push(node.left);
            if(node.right)
            queue.push(node.right);
        }
    }
    return output;


}
