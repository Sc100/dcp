
/*Given a binary tree root, return the sum of all values in the tree.

Constraints

n ≤ 100,000 where n is the number of nodes in root

*/

// Tree node looks like
// Node 
/* {
   val : val,
   left : left,
   right : right
}
*/


function solve(root) {
    return preorder(root);
  
 function preorder(root) {
    if (root == null) {
      return 0;
    }
    return root.val + this.preorder(root.left) + this.preorder(root.right);
  }
}

